<?php
$url='http://'.$_SERVER['HTTP_HOST'].'/login';
if(isset($redirect))
{
	$url=$url.'?redirect='.$redirect;
}
?>
<head>
	<title>Login</title>
</head>
<form method="post" action="<?= $url; ?>">
	<?php if(isset($errorMsg) && count($errorMsg)>0)
	{
		if(count(array_intersect($errorMsg, ['invalid username_password'])) == 1)
		{
			 echo "<p style=\"color: red;\">Invalid username or password</p>\n\n";
		}
	}
	?>
	<p>Username:</p>
	<?php if(isset($errorMsg) && count($errorMsg)>0)
	{
		if(count(array_intersect($errorMsg, ['no username'])) == 1)
		{
			 echo "<p style=\"color: red;\">*Please enter an username</p>\n\n";
		}
		else if(count(array_intersect($errorMsg, ['invalid username'])) == 1)
		{
			echo "<p style=\"color: red;\">*Invalid username. The username can only contain letters, numbers and underscore (_), and must be between 6 and 12 characters </p>\n\n";
		}
	}
	?>
	<input type="text" name="username" value="<?php if(isset($keepUser)){echo $keepUser;} ?>"/>
	<p>Password:</p>
	<?php if(isset($errorMsg) && count($errorMsg)>0)
	{
		if(count(array_intersect($errorMsg, ['no password'])) == 1)
		{
			 echo "<p style=\"color: red;\">*Please enter a password</p>\n\n";
		}
		else if(count(array_intersect($errorMsg, ['invalid password'])) == 1)
		{
			echo "<p style=\"color: red;\">*Invalid password. The password must be between 8 and 16 characters </p>\n\n";	
		}
	}
	?>
	<input type="Password" name="password" />
	<br/>
	
	<input type="submit" value="Submit">
</form>
