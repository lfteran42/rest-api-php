<?php

require_once(getcwd().'/Model/User.php');

class UserController
{

	/**
     * This method handles the request made to get all the users
     */
    public function index()
    {
        // getting all the users
        $users = User::allUsers();
        require_once('View/Users/index.php');
    }
    /**
     * This method handles the request made to get an specific user, identified by an id.
     * @param int id number that identifies the user in the application
     */
    Public function view($id){
        //find the specific user
    	$user=User::findUser($id);
        require_once('View/Users/view.php');
    }
    /**
     * This method handles the request made to create a new user.
     * @param string username name of the new user
     * @param string password password of the new user to authenticate 
     * @param mixed roles array with the roles for the new user.
     */
    public function addUser($username, $password, $roles)
    {
        //check if the username is already in use;
        $existingUser=User::findUserByName($username);
        if($existingUser !== NULL)
        {
            //If we got an error message, display it
            if(is_string($existingUser))
            {
                showError('', $existingUser);
                exit();
            }
            //else, the user already exists, so we cannot add it again
            else
            {
                $this->showError('', 'Username already in use');
                exit();
            }
        }
        //call add user method
        $user=new User($username, $password,  $roles);
        $result=User::addUser($user);
        //If the user couldn't be added, display the error
        if(is_string($result))
        {
            $this->showError('', $result);
            exit();
        }
        else
        {
            header('location: http://'.$_SERVER['HTTP_HOST'].'/users/');    
        }
    }
    /**
     * This method handles the request made to update the info of an specific user, identified by and id.
     * @param int id numeric identifier of the user.
     * @param string username name of the new user
     * @param string password password of the new user to authenticate 
     * @param mixed roles array with the roles for the new user.
     */
    public function updateUser($id, $username, $password, $roles)
    {
        $user=new user ($username, $password,  $roles);
        //check if the user exists
        $existingUser=User::findUser($id);
        if($existingUser !== NULL)
        {
            if(is_string($existingUser))
            {
                $this->showError('', $existingUser);
                exit();
            }
        }
        else
        {
            $this->showError('', 'User does not exists');
            exit();
        }

        //check if the username is in use
        $existingUsername=User::findUserByName($username);
        if($existingUsername !== NULL)
        {
            if(is_string($existingUsername))
            {
                $this->showError('', $existingUsername);
                exit();
            }
            else if ($username !== $existingUser->username)
            {
                $this->showError('', 'Username already in use');
                exit();
            }
        }
        $newUser=new User($username, $password, $roles, $id);
        //call update user method
        $result=User::updateUser($newUser);
        if(is_string($result))
        {
            $this->showError('', $result);
            exit();
        }
        else
        {
            header('location: http://'.$_SERVER['HTTP_HOST'].'/users/');   
        }
    }
    /**
     * This method handles the request made to delete an specific user, identified by an id.
     * * @param int id numeric identifier of the user.
     */
    Public function deleteUser($id){		
        //check if the user exists
        $existingUser=User::findUser($id);
        if($existingUser !== NULL)
        {
            if(is_string($existingUser))
            {
                $this->showError('', $existingUser);
                exit();
            }
        }
        else
        {
            $this->showError('', 'User does not exists');
            exit();
        }
        //call delete user method
        $result =User::deleteUser($id);
        header('location: http://'.$_SERVER['HTTP_HOST'].'/users/');   
    }

    /**
     * Function that validates if an user exists in the database (with an username and password), and if it has one or more roles. 
     * @param string username name of the user.
     * @param string password password of the user.
     * @param mixed roles array with the roles to be compared with the user's roles.
     * @return User|NULL returns the user object if the user is valid and has the roles; NULL otherwise.
     */
    public function validateUser($username, $password, $requiredRoles=array())
    {
        $user=User::findUserByName($username);
        //If we found an user with the provided name, check if the passwords are the same
        if($user!==NULL)
        {
            if($user->comparePassword($password))
            {
                //If we must validate the roles required from the user
                if(count($requiredRoles>0))
                {
                    //If the user contains all the roles required for this validation
                    if(count(array_intersect($requiredRoles, $user->roles)) == count($requiredRoles))
                    {
                        return $user;
                    }
                    else
                    {
                        return NULL;
                    }               
                }
                else
                {
                    return $user;    
                }
            }
            else
            {
                return NULL;
            }
        }
        return NULL;
    }
    /**
     * Function that logs in and user given an username and password, and create the user session. If the information is valid, initializes the session.
     * @return bool true if the information id valid, false otherwise.
     */
    Public function login($username, $password)
    {
        $validationResult=$this->validateUser($username,$password);
        if($validationResult)
        {            
            session_start();
            //initialize the session
            $_SESSION['username']=$validationResult->username;
            $_SESSION['roles']=$validationResult->roles;
            $_SESSION['user_id']=$validationResult->getId();
            $_SESSION['last_activity']= time();
            return true;   
        }
        return false;
    }
    /**
     * Function that logs out and username, deleting its session. Returns true if the session is deleted successfully; false otherwise.
     */
    Public function logout()
    {
        $_SESSION = [];
        if (session_status() == PHP_SESSION_ACTIVE) 
        {
            session_destroy(); 
            return true;
        }
        return false;
    }

    /**
     * Function that validates if there is an active session with the required information
     */
    public function isSessionStarted()
    {
        return (!empty($_SESSION) && isset($_SESSION['username']) && !empty($_SESSION['username']) && isset($_SESSION['roles']) && !empty($_SESSION['roles']) && isset($_SESSION['last_activity']) && !empty($_SESSION['last_activity']) && isset($_SESSION['user_id']) && !empty($_SESSION['user_id']));
    }

    /**
     * Function that displays an error page given an error title and error message.
     * @param string errorTitle title to be shown in th error page
     * @param string errorMessage message that describes the error
     */
    public function showError($errorTitle,$errorMessage)
    {
        $errorHeader = $errorTitle;
        $errorMsg = $errorMessage;
        require_once('View/error.php');
    }
}
?>