<?php
	require_once('connection.php');
	
	$db=new Db();
	
		//Validate if the user's table exists. If not, create it and fill it
		$sql='SELECT name FROM sqlite_master WHERE type="table" AND name="users";';
		$statement = $db->prepare($sql);
		$result=$statement->execute();
		$resultArray=$result->fetchArray();
		//check if the result set has any row
		if ($resultArray[0] != NULL) 
		{
			//close the statement result
			$result->finalize();
			//table exists, drop the table
			$sql='DROP TABLE users;';
			$statement = $db->prepare($sql);
			$result=$statement->execute();
		} 

		//Create the user's table
		$sql_create_table_users = '
		CREATE TABLE users (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			username TEXT NOT NULL,
			password TEXT NOT NULL,
			roles TEXT,
			CONSTRAINT username_unique UNIQUE (username)
		);';
		$result=$db->exec($sql_create_table_users);

		//insert the user data
		$sql_insert='INSERT INTO users (username,password,roles) VALUES 
		("testuser1","'.md5('testpassword1' ).'","PAGE_1"),
		("testuser2","'.md5('testpassword2' ).'","PAGE_2"),
		("testuser3","'.md5('testpassword3' ).'","PAGE_3"),
		("testuser4","'.md5('testpassword4' ).'","PAGE_2,PAGE_1"),
		("testuser5","'.md5('testpassword5' ).'","PAGE_3,ADMIN"),
		("testuserAdmin","'.md5('passwordadmin' ).'","ADMIN");';
		$result=$db->exec($sql_insert);   
		
		$db->close();
?>