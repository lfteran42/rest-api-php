# README #

This is a web application made with PHP and SQLite that implements users with roles to access private pages and other functionalities. The application manages 3 private pages for 3 different roles:

* **page_1:** requires the user to have the role PAGE_1.
* **page_2:** requires the user to have the role PAGE_2.
* **page_3:** requires the user to have the role PAGE_3.

Each page has a message of “Hello <USERNAME>” and a link to close the user session. Each user session has an expiration time of 5 minutes since the last user action.

This project also implements a simple REST API for managing the Users resource. To authenticate, you use the same credentials as the ones used in the login form using using HTTP basic authentication.
To create, update or delete users, you must authenticate with an user with the ADMIN role. The other users can only read through this API.

### How to install ###

* Set up
Download the project from the repository, open a terminal on the directory with the application files, and run the command:  
`php -S localhost:8000`
This will start the PHP built-in server with the application files. Entering `localhost:8000` on an internet browser will trigger the `install.php` file to be called the first time, which will create the database and the users (if you want to reset the database, you can rename the file `install.php.bak` to `install.php`).

* Dependencies
The application uses https://github.com/nikic/FastRoute for routing the requests received.

### How to use ####

The users are defined with 4 attributes:

* *Id:* A number that identifies the user in the database.
* *Username:* The name of the user. Each username is unique and can only contain letters, numbers and underscore (_), and must be between 6 and 12 characters.
* *Password:* A string used to authenticate the user when starting a session or accessing the REST API. a password must be between 8 and 16 characters.
* *Roles:* A list of all the roles that the user has. Each role gives access to different private pages. The ADMIN role gives access to the create, update and delete endpoints of the REST API.

The application starts with 6 users:

* *username:* testuser1, *password:* testpassword1, and *roles:* PAGE_1.
* *username:* testuser2, *password:* testpassword2, and *roles:* PAGE_2.
* *username:* testuser3, *password:* testpassword3, and *roles:* PAGE_3.
* *username:* testuser4, *password:* testpassword4, and *roles:* PAGE_2, PAGE_1.
* *username:* testuser5, *password:* testpassword5, and *roles:* PAGE_3, ADMIN.
* *username:* testuserAdmin, *password:* passwordadmin, and *roles:* ADMIN.

* Private pages

To access a private page you can either enter `localhost:8000`, `localhost:8000/page_1/`, `localhost:8000/page_2/`, or `localhost:8000/page_3` on an internet browser, and the application will load to the login form to initiate a session, and after authenticating, you will be redirected to the previously requested page.
If you start a session with an user which doesn't have access from the `localhost:8000`, you will be redirected to a private page associated with the user role. If you try to access a page which you don't access (i.e. starting a session with an user with only ROLE_1 and trying to access `page_3`), the server will return error `401 Unauthorized`.

* REST API

There are 5 request that can be send to the application to access the Users resourse:

* **List users:** Send a GET request to `localhost:8000/users/`. Returns a list with JSON object with the id, username and roles of all the users.
* **Get user:** Send a GET request to `localhost:8000/users/<id>`, where `<id>` is the id that identifies the user. Returns a JSON object with the user id, username and roles.
* Create user: Send a POST request to `localhost:8000/users/` and a JSON object with an username, password and a list of roles; i.e.: {"username":"newuser", "password":"newpassword", "roles":["PAGE_1","PAGE_3"]}. 
* **Update user:** Send a PATCH request to `localhost:8000/users/<id>`, where `<id>` is the id that identifies the user to update, and a JSON object with the new username, password and roles; i.e.: {"username":"updateduser", "password":"udatedpassword", "roles":["PAGE_2","ADMIN"]}. 
* **Delete user:** Send a DELETE request to `localhost:8000/users/<id>` , where `<id>` is the id that identifies the user to delete.

If you try to access an nonexistant resource, an unsupported request (a DELETE request to `localhost:8000/users/`), or a malformed request (try to create an user without a JSON object), the application will return error `404 Not Found`, `405 Method Not Allowed` and `400 Bad Request` respectively.

### contact information ###

Email: lfteran42@gmail.com