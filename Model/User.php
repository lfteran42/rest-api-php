<?php

/**
 * Class that represents an user with its corresponding attributes.
 */
class User implements JsonSerializable 
{
	//Unique number that identifies the user on the application.
	private $id;
	//Name that identifies the user.
	public $username;
	//Array of roles assigned to the user. A role will determine which functionalities the user can access.
	public $roles;
	//Password used to authenticate the user.
	private $password;
		
	/**
	 * Constructor
	 * @param string username string that identifies the user
	 * @param string password string used by the user to authenticate in the application
	 * @param mixed roles array with the roles assigned to the user.
	 * @param int id value. Represents a number that identifies the user in the application
	 */
	public function __construct($username, $password, $roles, $id = 0)
	{
		$this->id = $id;
		$this->username = $username;
		$this->password = $password;
		$this->roles = $roles;
	}
	public function jsonSerialize() {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'roles' => $this->roles
        ];
    }
	/**
	 * Function to get the user id number.
	 * @return int the user id.
	 */
	public function getId()
	{
		return $this->id;
	}
	/**
 	 * Function that updates the user's password.
	 */
	public function setPassword($password)
	{
		$this->password=$password;
		
	}
	/**
	 * Function that compares the user's password with the current password
	 */
	public function comparePassword($password)
	{
		return (strcmp($this->password,$password) === 0);	
	}
	
	/**
	 * Get a list with all the users
	 */
	public static function allUsers()
	{
		$result=[];
		
		//load the list of users
		$db=new Db();
		if(!$db) {
    	   	return 'There was an error connecting to the database';
  	  	} else {
            $sql ='SELECT id, username, roles from users;';
	   	    $query=$db->query($sql);
	   	    while($row = $query->fetchArray(SQLITE3_ASSOC) ) 
	   	    {
	   	    	//convert the roles string into an array.
				$roles=explode(',', $row["roles"]);
				asort($roles);
				$result[]=new user($row["username"], "", $roles, $row["id"]);
	    	}

	   	 	$db->close();
			return $result;
   	 	}
	}
	
	/**
	 * Get an user given an id.
	 * @param int id number that identifies the user
	 * @return User|string|NULL returns the user object if found, string if there is an error or, NULL if the user is not found.
	 */
	public static function findUser($id)
	{
		$result = NULL;
		// check that the $id is an integer.
		$id = intval($id);
		
		//search  for the user.
		$db=new Db();
		if(!$db) {
       		return 'There was an error connecting to the database';
  	  	} else {
        	$statement = $db->prepare('SELECT id, username, password, roles FROM users WHERE id = :id;');
			$statement->bindValue(':id', $id);
			$result = $statement->execute();
			$result->finalize();
			while ($row = $result->fetchArray(SQLITE3_ASSOC)) 
			{
				//convert the roles string into an array.
				$roles=explode(',', $row["roles"]);
				asort($roles);
				$user=new user($row["username"], $row["password"], $roles, $row["id"]);
			    $db->close();
			    return $user;
			}
			$db->close();
			return NULL;
   	 	}
	}
	/**
	 * Get an user given an username.
	 * @param string username name of the user.
	 * @return User|string|NULL returns the user object if found, string if there is an error or, NULL if the user is not found.
	 */
	public static function findUserByName($username)
	{
		$result = NULL;
		
		//search for the user.
		$db=new Db();
		if(!$db) {
       		return 'There was an error connecting to the database';
  	  	} else {
        	$statement = $db->prepare('SELECT id, username, roles, password FROM users WHERE username = :username;');
			$statement->bindValue(':username', $username);
			$resultSet = $statement->execute();
		   	if(!$resultSet)
		   	{
		   		$db->close();
				return 'there was and error retrieving the user';
		   	}
			while ($row = $resultSet->fetchArray(SQLITE3_ASSOC)) 
			{
				//convert the roles string into an array.
				$roles=explode(',', $row["roles"]);
				asort($roles);
			    $user=new user($row["username"], $row["password"], $roles, $row["id"]);
			    $resultSet->finalize();
			    $db->close();
			    return $user;
			}
			$db->close();
			return NULL;
   	 	}
	}	
	/**
	 * Adds the provided user to the database.
	 * @param User user user object with the data to be added.
	 * @return bool|string returns true if the user is added; otherwise returns the error message.
	 */
	public static function addUser($user)
	{	
		$db=new Db();
		if(!$db) {
       		return 'There was an error connecting to the database';
  	  	} else {
        	$statement = $db->prepare('INSERT INTO users (username,password,roles) VALUES (:username,:password,:roles);');
			$statement->bindValue(':username', $user->username);
			$statement->bindValue(':password', $user->password);
			//convert the roles array into a string for storing.
			$roles=implode(',', $user->roles);
			$statement->bindValue(':roles', $roles);
			$result = $statement->execute();
		   	
		   	//if there was an error adding the new user.
		   	if(!$result)
		   	{
		   		$db->close();
		   		return 'there was and error adding the user';
		   	}
		   	$result->finalize();
		   	$db->close();
		   	return true;
   	 	}
	}
	/**
	 * Updates an user in the database with the provided info.
	 * @param User user user object with the data to be updated. The id is the id of the user to be modified.
	 * @return bool|string returns true if the user is updated; otherwise returns the error message.
	 */
	public static function updateUser($newUserData)
	{
		//try to update the user
		$db=new Db();
		if(!$db) {
       		return 'There was an error connecting to the database';
  	  	} else {
        	$statement = $db->prepare('UPDATE users SET username=:username, password=:password, roles=:roles WHERE id=:id;');
			$statement->bindValue(':username', $newUserData->username);
			$statement->bindValue(':password', $newUserData->password);
			//convert the roles array into a string for storing
			$roles=implode(',', $newUserData->roles);
			$statement->bindValue(':roles', $roles);
			$statement->bindValue(':id', $newUserData->getId());
			$result = $statement->execute();
		   	//if there was an error updating the user
		   	if(!$result)
		   	{
		   		$db->close();
		   		return 'there was and error updating the user';
		   	}
		   	$result->finalize();
		   	$db->close();
		   	return true;
   	 	}
   	 
	   	
	}
	/**
	 * Deletes an user in the database.
	 * @param int id id of the user to be updated. 
	 * @return bool|string returns true if the user is deleted; otherwise returns the error message.
	 */
	public static function deleteUser($id)
	{
		// try to delete the user to the storage
		$db=new Db();
		if(!$db) {
       		return 'There was an error connecting to the database';
  	  	} else {
        	$statement = $db->prepare('DELETE FROM users WHERE id=:id;');
			$statement->bindValue(':id', $id);
			$result = $statement->execute();
		   	//if there was an error deleting
		   	if(!$result)
		   	{
		   		$db->close();
		   		return 'there was and error deleting the user';
		   	}
		   	$result->finalize();
		   	$db->close();
		   	return true;
   	 	}
	}
}	
?>
