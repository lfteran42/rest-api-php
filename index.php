<?php
	/**
	 * Index file of the application. Uses nikic/FastRoute library for handling the routing of the user's requests
	 */
	require 'vendor/autoload.php';
	require_once('config.php');
	require_once('connection.php');
	require_once('Controller/UserController.php');

	//if the file install.php exists, run it one and rename it so it doesnt re-initialize the database each time a new request is done
	if(file_exists(getcwd(). DIRECTORY_SEPARATOR .'install.php' ))
	{
		
		require_once('install.php' );
		rename('install.php','install.php.bak');
	}
	
	$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r) 
	{
		//validation and pages handlers
	    $r->addRoute('GET', '/', 'validate_user_handler');
		$r->addRoute('GET', '/login[/]', 'login_user_handler');
		$r->addRoute('POST', '/login[/]', 'login_user_handler');
		$r->addRoute('GET', '/logout[/]', 'logout_user_handler');
		$r->addRoute('GET', '/page_1[/]', 'page_handler');
		$r->addRoute('GET', '/page_2[/]', 'page_handler');
		$r->addRoute('GET', '/page_3[/]', 'page_handler');
		//REST API handlers
	    $r->addRoute('GET', '/users[/]', 'get_users_handler');
		$r->addRoute('POST', '/users[/]', 'create_user_handler');	    
	    $r->addRoute('GET', '/users/{id:\d+}', 'get_user_handler');
	    $r->addRoute('PATCH', '/users/{id:\d+}', 'update_user_handler');
	    $r->addRoute('DELETE', '/users/{id:\d+}', 'delete_user_handler');
    });
    
    //Controller class used for managing the users
	$userController= new UserController();
    // Fetch HTTP method and URI
	$httpMethod = $_SERVER['REQUEST_METHOD'];
	$uri = $_SERVER['REQUEST_URI'];
	// Strip query string and decode URI
	if (false !== $pos = strpos($uri, '?')) 
	{
	    $uri = substr($uri, 0, $pos);
	}
	$uri = rawurldecode($uri);
	$routeInfo = $dispatcher->dispatch($httpMethod, $uri);
	switch ($routeInfo[0]) 
	{
	    case FastRoute\Dispatcher::NOT_FOUND:
	        header("HTTP/1.1 404 Not Found");
	        $userController->showError('404 Not Found','');
	        break;
	    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
	        $allowedMethods = $routeInfo[1];
	        header("HTTP/1.1 405 Method Not Allowed");
	        $userController->showError('405 Method Not Allowed','');
	        break;
	    case FastRoute\Dispatcher::FOUND:
	        $handler = $routeInfo[1];
	        $vars = $routeInfo[2];
	        switch ($handler)
	        {
	        	/**
				 * Handler that manages the request made to the root address of the application. 
				 * Validates if there is an user session started and has not expired. If the session is still valid, redirects to the corresponding page; 
				 * otherwise, delete the session (if any) and load the login page. If there is no session, redirect to the login page with the redirect URI
	        	 */
	        	case 'validate_user_handler':
	        		//check if the session is already started
	        	    session_start();
	        		if($userController->isSessionStarted()) 
	        		{
        				//check if the session expired
        				$expirationTime = $_SESSION['last_activity'] + (5 * 60);
        				if( time() > $expirationTime)
        				{
        					//the session expired
    						$logoutResult=$userController->logout();
    						header('location: http://'.$_SERVER['HTTP_HOST'].'/login/?redirect=/');
        				}
        				else
        				{
        					//renew the session timeout
        					$_SESSION['last_activity']=time();
        					$roles = $_SESSION['roles'];
        					//redirect to a private page based on the user's role
	        				foreach ($roles as $role) 
	        				{
	        					switch ($role) 
	        					{
	        						case 'PAGE_1':
	        							header('location: http://'.$_SERVER['HTTP_HOST'].'/page_1/');
	        							exit();
	        							break;
	        						case 'PAGE_2':
	        							print_r('go page 2');
	        							header('location: http://'.$_SERVER['HTTP_HOST'].'/page_2/');
	        							exit();
	        							break;
	        						case 'PAGE_3':
	        							print_r('go page 3');
	        							header('location: http://'.$_SERVER['HTTP_HOST'].'/page_3/');
	        							exit();
	        							break;
	        					}
	        				}
	        				//if the user does not have any valid role
        					header("HTTP/1.1 401 Unauthorized");
        					$userController->showError('401 Unauthorized','');
        				}
	        		}
	        		else
	        		{
	        			//user not authenticated
	        			header('location: http://'.$_SERVER['HTTP_HOST'].'/login/?redirect=/');
	        		}
	        		break;
	        		/**
					 * Handler that manages the request made to the private pages in the application. 
					 * Validates if there is an user session started and has not expired. If the session is still valid, load the corresponding page; 
					 * otherwise, delete the session (if any) and redirects the login page. If there is no session, redirect to the login page with the redirect URI
		        	 */
	        		case 'page_handler':
	        			//check if the session is already started
		        	    session_start();
		        		if($userController->isSessionStarted()) 
		        		{
	        				//check if the session expired
	        				$expirationTime = $_SESSION['last_activity'] + (5 * 60);
	        				if( time() > $expirationTime)
	        				{
	        					//the session expired
	    						$logoutResult=$userController->logout();
	    						header('location: http://'.$_SERVER['HTTP_HOST'].'/login/?redirect='.$uri);
	        				}
	        				else
	        				{
	        					//renew the session timeout
	        					$_SESSION['last_activity']=time();
	        					$roles = $_SESSION['roles'];
		        				foreach ($roles as $role) 
		        				{
		        					switch ($role) 
		        					{
		        						case 'PAGE_1':
		        							if(preg_match('/^(\\/page_1)[\\/]?$/',$uri))
		        							{
		        								require_once('View/Pages/page_1.php');	
		        								exit();
		        							}
		        							break;
		        						case 'PAGE_2':
		        							if(preg_match('/^(\\/page_2)[\\/]?$/',$uri))
		        							{
		        								require_once('View/Pages/page_2.php');	
		        								exit();
		        							}
		        							break;
		        						case 'PAGE_3':
		        							if(preg_match('/^(\\/page_3)[\\/]?$/',$uri))
		        							{
		        								require_once('View/Pages/page_3.php');	
		        								exit();
		        							}
		        							break;
		        					}
		        				}
		        				//if the user does not have any valid role
	        					header("HTTP/1.1 401 Unauthorized");
	        					$userController->showError('401 Unauthorized','');
	        				}
		        		}
		        		else
		        		{
		        			//user not authenticated
		        			header('location: http://'.$_SERVER['HTTP_HOST'].'/login/?redirect='.$uri);
		        		}
		        		break;

		        	/**
					 * Handler that manages the request to the login form. 
					 * If the user sends a POST request, validate the information received and try to log in the user and redirect to the previously requested page
					 * If the user sends a GET request, then load the login form.
		        	 */
	        		case 'login_user_handler':
	        			$redirect='';
	        			if(isset($_GET['redirect']))
	        			{
	        				$redirect=$_GET['redirect'];
	        			}
	        			//if it's a POST request
	        			if(isset($_POST['username']) && isset($_POST['password']))
	        			{
	        				$username=$_POST['username'];
	        				$password=$_POST['password'];
	        				$errorMsg=[];
	        				//we keep the username for the username field in case there was any error.
	        				$keepUser=$username;
	        				//validate the username and password
	        				if(empty($username))
	        				{
	        					$errorMsg[]="no username";	
	        				}
	        				if(empty($password))
	        				{
	        					$errorMsg[]="no password";
	        				}
	        				if(!preg_match('/^[A-Za-z0-9_]+$/',$username) || strlen($username)<6 || strlen($username)>12)
	        				{
	        					$errorMsg[]="invalid username";
	        					$keepUser='';
	        				}
	        				if(strlen($password)<8 || strlen($password)>16)
	        				{
	        					$errorMsg[]="invalid password";
	        				}
	        				if(count($errorMsg)>0)
	        				{
	        					require_once('View/Pages/login.php');
	        				}
	        				else
	        				{
	        					$loginResult=$userController->login($username,md5($password));
        						//if the user authenticated successfully, redirect to the previously request page
        						if($loginResult)
        						{
        							header('location: http://'.$_SERVER['HTTP_HOST'].''.$redirect);	
        						}
        						else
        						{
        							$errorMsg[]="invalid username_password";		
        							require_once('View/Pages/login.php');
        						}
	        				}
	        			}
	        			//if it is a GET request, load the login page
	        			else
	        			{
	        				require_once('View/Pages/login.php');
	        			}
	        			break;
	        		/**
					 * handler that manages the request to logout the user. 
					 * deletes the currently logged in user session and redirects to the root page of the application
		        	 */
        			case 'logout_user_handler':
        				session_start();
    					$logoutResult=$userController->logout();
        				header('location: http://'.$_SERVER['HTTP_HOST']);			
	        			break;
	        		/**
					 * Handler that manages the request for a list of all users, displaying their id, username, and roles in JSON format.
					 * To get the list of users, the user must be authenticated using HTTP basic authentication.
		        	 */
	        		case 'get_users_handler':
	        			//if the user send its authentication information through HTTP basic authentication.
	        			if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW']))
	        			{
	        				//validate the user info
	        				$validUser=$userController->validateUser($_SERVER['PHP_AUTH_USER'],$_SERVER['PHP_AUTH_PW']);
							if($validUser)
	        				{
	        					$users = $userController->index();
	        				}
	        				else
		        			{
		        				header("HTTP/1.1 401 Unauthorized");
		        				$userController->showError('401 Unauthorized','');
		        			}
	        			}
	        			else
	        			{
	        				header("HTTP/1.1 401 Unauthorized");
	        				$userController->showError('401 Unauthorized','');
	        			}
	        			break;
	        		/**
					 * Handler that manages the request for an specific user (identified by an id), displaying their id, username, and roles in JSON format.
					 * To get the info about the requested user, the user must be authenticated using HTTP basic authentication.
		        	 */
	        		case 'get_user_handler':
	        			//if the user send its authentication information through HTTP basic authentication.
	        			if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW']))
	        			{
	        				$userController->view($vars['id']);
	        			}
	        			else
	        			{
	        				header("HTTP/1.1 401 Unauthorized");
	        				$userController->showError('401 Unauthorized','');
	        			}
	        			break;
	        		/**
					 * Handler that manages the request to create a new user (requiring an username, password and roles).
					 * To create an user, the requesting user must be authenticated using HTTP basic authentication and have the ADMIN role.
		        	 */
	        		case 'create_user_handler':
	        			//if the user send its authentication information through HTTP basic authentication.
	        			if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW']))
	        			{
	        				//validate the user info
	        				$validUser=$userController->validateUser($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'], ["ADMIN"]);
	        				if($validUser)
	        				{
	        					$jsonData = json_decode(file_get_contents('php://input'), true);
	        					//validate the information for the new user
	        					if(isset($jsonData) && isset($jsonData['username']) && isset($jsonData['password']) && isset($jsonData['roles'])) 
			        			{
			        				print_r('enter in array');
			        				print_r($jsonData);
			        				$username=$jsonData['username'];
			        				$password=$jsonData['password'];
			        				$roles=$jsonData['roles'];
									//check if the username and passwords are valid
									if(!preg_match('/^[A-Za-z0-9_]+$/',$username) || strlen($username)<6 || strlen($username)>12)
			        				{
			        					$userController->showError('Invalid username', 'The username can only contain letters, numbers and underscore (_), and must be between 6 and 12 characters');
			        					exit();
			        				}
			        				if(strlen($password)<8 || strlen($password)>16)
			        				{
			        					$userController->showError('Invalid password', 'The password must be between 8 and 16 characters');
			        					exit();
			        				}
			        				//if the roles are received as a string, convert to an array
			        				if(is_string($roles))
			        				{
			        					$roles = preg_replace('/\s+/','',$roles);
			        					$roles=explode(',',$roles);
			        				}
		        					$userController->addUser($username, md5($password), $roles);
			        			}
			        			else
			        			{
			        				header("HTTP/1.1 400 Bad Request");
			        				$userController->showError('400 Bad Request','');
			        			}
	        				}
	        				else
	        				{
	        					header("HTTP/1.1 401 Unauthorized");
	        					$userController->showError('401 Unauthorized','');
	        				}
	        			}
	        			else
	        			{
	        				header("HTTP/1.1 401 Unauthorized");
	        				$userController->showError('401 Unauthorized','');
	        			}
	        			break;
	        		/**
					 * Handler that manages the request to update the info of an existing user identified by and id, 
					 * and requiring the new username, new password and new roles.
					 * To update an user, the requesting user must be authenticated using HTTP basic authentication and have the ADMIN role.
		        	 */
	        		case 'update_user_handler':
	        			//if the user send its authentication information through HTTP basic authentication.
	        			if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW']))
	        			{
	        				//validate the user info
	        				$validUser=$userController->validateUser($_SERVER['PHP_AUTH_USER'],$_SERVER['PHP_AUTH_PW'], ["ADMIN"]);
	        				if($validUser)
	        				{
	        					//retrieve the new user info
	        					$jsonData = json_decode(file_get_contents('php://input'), true);
	        					//validate the information for the new user
	        					if(isset($jsonData) && isset($jsonData['username']) && isset($jsonData['password']) && isset($jsonData['roles'])) 
			        			{
			        				$id = $vars['id'];
			        				$username=$jsonData["username"];
			        				$password=$jsonData["password"];
			        				$roles=$jsonData['roles'];
			        				//check if the username and passwords are valid
									if(!preg_match('/^[A-Za-z0-9_]+$/',$username) || strlen($username)<6 || strlen($username)>12)
			        				{
			        					$userController->showError('Invalid username','The username can only contain letters, numbers and underscore (_), and must be between 6 and 12 characters');
			        					exit();
			        				}
			        				if(strlen($password)<8 || strlen($password)>16)
			        				{
			        					$userController->showError('Invalid password', 'The password must be between 8 and 16 characters');
			        					exit();
			        				}

			        				//if the roles are received as a string, convert to an array
			        				if(is_string($roles))
			        				{
			        					$roles = preg_replace('/\s+/','',$roles);
			        					$roles = explode(',',$roles);
			        				}
		        					$userController->updateUser($id, $username, md5($password), $roles);
			        			}
			        			else
			        			{
			        				header("HTTP/1.1 400 Bad Request");
			        				$userController->showError('400 Bad Request','');
			        			}
	        				}
	        				else
	        				{
	        					header("HTTP/1.1 401 Unauthorized");
	        					$userController->showError('401 Unauthorized','');
	        				}
	        			}
	        			else
	        			{
	        				header("HTTP/1.1 401 Unauthorized");	
	        				$userController->showError('401 Unauthorized','');
	        			}
	        			break;
	        		/**
					 * Handler that manages the request to delete an existing user identified by and id.
					 * To delete an user, the requesting user must be authenticated using HTTP basic authentication and have the ADMIN role.
		        	 */
	        		case 'delete_user_handler':
	        			//if the user send its authentication information through HTTP basic authentication.
	        			if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW']))
	        			{
	        				//validate the user info
	        				$validUser = $userController->validateUser($_SERVER['PHP_AUTH_USER'],$_SERVER['PHP_AUTH_PW'], ["ADMIN"]);
	        				if($validUser)
	        				{
        						$id = $vars['id'];
    							$userController->deleteUser($id);		
	        				}
	        				else
	        				{
	        					header("HTTP/1.1 401 Unauthorized");
	        					$userController->showError('401 Unauthorized','');
	        				}
	        			}
	        			else
	        			{
	        				header("HTTP/1.1 401 Unauthorized");	
	        				$userController->showError('401 Unauthorized','');
	        			}
        				break;
	        }
	        break;
	}
?>